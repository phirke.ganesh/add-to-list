/**
 * Created by gphirke on 6/26/2018.
 */

import React  from 'react';
import ListItem from './ListItem.js'

export default  class ToDoList extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextProps) {
        debugger;
    }

    render() {
        const items = this.props.items;
        return (
            <ul>
                {
                    items.map((item, i) => <ListItem key={i} value={item}/>)
                }
            </ul>
        )
    }

}