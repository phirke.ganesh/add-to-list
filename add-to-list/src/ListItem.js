/**
 * Created by gphirke on 6/26/2018.
 */

import React  from 'react';

export default class ListItem extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            value : props.value
        }
    }

    render(){
        const item = this.props.value;
        return (
            <li>{item}</li>
        )
    }

}
