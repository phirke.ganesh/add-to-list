import React, { Component } from 'react';
///import logo from './logo.svg';
import './App.css';
import ToDoList from './ToDoList.js'
class App extends Component {

   constructor(props){
        super(props);
        this.state = {
            inputValue: '',
            items: ['Apple', 'banana']
        }
    }

    onClick = () => {
        const { inputValue, items } = this.state;
        if (inputValue) {
        const nextState = [...items, inputValue];
        this.setState({ items: nextState, inputValue: '' });
        }
    }

    onChange = (e) => this.setState({ inputValue: e.target.value });

    handleItemClick = (e) => {console.log(e.target.innerHTML)}


    render() {
        const {inputValue } = this.state;
        return (
            <div>
                <input type="text" value={inputValue} onChange={this.onChange} />
                <button onClick={this.onClick}>Add</button>
                <ToDoList items={this.state.items} />
            </div>
        );
    }

}

export default App;
